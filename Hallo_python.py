#Sample Hallo Python program to build using Jenkins

from datetime import datetime

# datetime object containing current date and time

print('Hi There. This is my first Jenkins Build')

now = datetime.now()
 
print("now =", now)

# dd/mm/YY H:M:S
dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
print("date and time =", dt_string)	